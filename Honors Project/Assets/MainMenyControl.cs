﻿using UnityEngine;
using System.Collections;

public class MainMenyControl : MonoBehaviour 
{
	public void HordeMode()
	{
		Application.LoadLevel ("PVEMenu");
	}

	public void TeamDeathmatch()
	{
		Application.LoadLevel ("PVPMenu");
	}

	public void Tutorial ()
	{
		Application.LoadLevel ("Tutorial");
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void MainMenu()
	{
		Application.LoadLevel ("MainMenu");
	}
}
