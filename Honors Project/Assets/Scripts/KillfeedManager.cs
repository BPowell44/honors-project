﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;  //you will need this to create a List!
using UnityEngine.Networking;

public class KillfeedManager : NetworkBehaviour
{

    public List<KillFeedInfo> KF = new List<KillFeedInfo>();
    public static KillfeedManager instance;
    public int maxFeeds = 4; //After what ammount of Killfeed Messages the First should dissapear

    void Start()
    {
        instance = this;
    }

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(Screen.width - 250, 0, 400, 200));
        foreach (KillFeedInfo k in KF)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(k.Killer + " Killed " + k.Killed);
            GUILayout.EndHorizontal();
        }
        GUILayout.EndArea();
    }

    [Command]
    public void CmdServer_SendKillFeed(string Killer, string Killed)
    {
        if (isServer) RpcClient_SendKillFeed(Killer, Killed); // do not change isServer otherwise the Killfeed will be created twice!
    }

    [ClientRpc]
    public void RpcClient_SendKillFeed(string Killer, string Killed)
    {
        ConvertParamsToClass(Killer, Killed);
    }

    private void ConvertParamsToClass(string Killer, string Killed)
    {
        KillFeedInfo k = new KillFeedInfo();
        k.Killer = Killer;
        k.Killed = Killed;
        KF.Add(k);
        if (KF.Count > maxFeeds)
        {
            KF.RemoveAt(0); //if there are more KillFeedMessages than the max ammount delete the first!
        }
    }
}

[System.Serializable]
public class KillFeedInfo
{
    public string Killer;
    public string Killed;
}