﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerShooting : NetworkBehaviour
{
    // Variables
    public int shootingDamage;
    public int meleeDamage = 40;
    public float shootingRange = 200;
    public float meleeRange = 1;
    [SerializeField]
    private Transform camTransform;
    private RaycastHit hit;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public GameObject bulletHole;
    private Text ammoText;
    private Text gunChoiceText;
    private string weaponChoice;
    private static int magAmmo = 11;
    private static int spareAmmo = 33;
    [SyncVar]
    private int piMagSize = 11;
    [SyncVar]
    private int piMagAmmo = 11;
    [SyncVar]
    private int piSpareAmmo = 33;
    [SyncVar]
    private int piAllAmmo;
    [SyncVar]
    private int piMaxAmmo = 40;
    [SyncVar]
    private int mgMagSize = 30;
    [SyncVar(hook = "OnAmmoChange")]
    private int mgMagAmmo = 30;
    [SyncVar]
    private int mgSpareAmmo = 90;
    [SyncVar]
    private int mgAllAmmo;
    [SyncVar]
    private int mgMaxAmmo = 130;
    [SyncVar]
    private int shMagSize = 7;
    [SyncVar]
    private int shMagAmmo = 7;
    [SyncVar]
    private int shSpareAmmo = 35;
    [SyncVar]
    private int shAllAmmo;
    [SyncVar]
    private int shMaxAmmo = 42;
    private int selectedWeaponMag = 11;
    private bool canBuyAmmo = false;
    private AmmoBoxScript ammoBoxScript;
    private CrowdApprovalSystem crowdApprovalSystem;
    private bool isMeleeing;
    public bool shootingCrowd = false;
    public LayerMask layerMask;
    public Transform myTransform;
    public bool isAimingDownSight;
    public float weaponSpray = 0.2f;
    public float recoilAmount;

    private WeaponADS weaponADS;
    private GrenadeThrow grenadeThrow;

    private float mgFireRate = 0.1f;
    private float mgLastShot = 0.0f;
    private float shFireRate = 0.8f;
    private float shLastShot = 0.0f;

    void Start()
    {
        if (isLocalPlayer)
        {
            ammoText = GameObject.Find("Ammo").GetComponent<Text>();
            gunChoiceText = GameObject.Find("Gun Choice").GetComponent<Text>();
            crowdApprovalSystem = GameObject.Find("GameManager").GetComponent<CrowdApprovalSystem>();
            weaponADS = transform.GetComponentInChildren<WeaponADS>();
            grenadeThrow = GetComponent<GrenadeThrow>();
            weaponChoice = "Pistol";
            ammoCheck();
            SwapWeapons();
            ammoText.text = "Ammo Count: " + magAmmo.ToString() + "/" + spareAmmo.ToString();
            myTransform = transform;

            // Reverse the layermask
            layerMask = ~layerMask;

        }
    }

    // Update is called once per frame+

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        // If shooting
        CheckIfShooting();
        ammoCheck();
        SwapWeapons();
        DisplayAmmo();
        CheckIfMeleeAttacking();
    }

    [Client]
    void OnAmmoChange(int gunAmmo)
    {
        mgMagAmmo = gunAmmo;
        DisplayAmmo();
    }

    void ammoCheck()
    {

        if (Input.GetKeyDown(KeyCode.R) && weaponChoice == "Pistol")
        {
            piAllAmmo = piMagAmmo + piSpareAmmo;

            if (piAllAmmo == 0)
            {
                // play noise - needs to be added in
            }

            else if (piAllAmmo < piMagSize)
            {
                piMagAmmo = piAllAmmo;
                piSpareAmmo = 0;
            }

            else
            {
                piSpareAmmo = piAllAmmo;
                piSpareAmmo -= piMagSize;
                piMagAmmo = piMagSize;
            }

            magAmmo = piMagAmmo;
            spareAmmo = piSpareAmmo;
            DisplayAmmo();
        }

        if (Input.GetKeyDown(KeyCode.R) && weaponChoice == "Machine Gun")
        {
            mgAllAmmo = mgMagAmmo + mgSpareAmmo;

            if (mgAllAmmo == 0)
            {
                // play noise - needs to be added in
            }

            else if (mgAllAmmo < mgMagSize)
            {
                mgMagAmmo = mgAllAmmo;
                mgSpareAmmo = 0;
            }

            else
            {
                mgSpareAmmo = mgAllAmmo;
                mgSpareAmmo -= mgMagSize;
                mgMagAmmo = mgMagSize;
            }

            magAmmo = mgMagAmmo;
            spareAmmo = mgSpareAmmo;
            DisplayAmmo();
        }

        if (Input.GetKeyDown(KeyCode.R) && weaponChoice == "Shotgun")
        {
            shAllAmmo = shMagAmmo + shSpareAmmo;

            if (shAllAmmo == 0)
            {
                // play noise - needs to be added in
            }

            else if (shAllAmmo < shMagSize)
            {
                shMagAmmo = shAllAmmo;
                shSpareAmmo = 0;
            }

            else
            {
                shSpareAmmo = shAllAmmo;
                shSpareAmmo -= shMagSize;
                shMagAmmo = shMagSize;
            }

            magAmmo = shMagAmmo;
            spareAmmo = shSpareAmmo;
            DisplayAmmo();
        }
    }

    void DisplayAmmo()
    {
        if (weaponChoice == "Pistol")
        {
            magAmmo = piMagAmmo;
            spareAmmo = piSpareAmmo;
            ammoText.text = "Ammo Count: " + magAmmo.ToString() + "/" + spareAmmo.ToString();
            selectedWeaponMag = piMagAmmo;
        }

        if (weaponChoice == "Machine Gun")
        {
            magAmmo = mgMagAmmo;
            spareAmmo = mgSpareAmmo;
            ammoText.text = "Ammo Count: " + magAmmo.ToString() + "/" + spareAmmo.ToString();
            selectedWeaponMag = mgMagAmmo;
        }

        if (weaponChoice == "Shotgun")
        {
            magAmmo = shMagAmmo;
            spareAmmo = shSpareAmmo;
            ammoText.text = "Ammo Count: " + magAmmo.ToString() + "/" + spareAmmo.ToString();
            selectedWeaponMag = shMagAmmo;
        }
    }


    public void SwapWeapons()
    {
        gunChoiceText.text = "Current Weapon: " + weaponChoice;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            weaponChoice = "Pistol";
            gunChoiceText.text = "Current Weapon: " + weaponChoice;
            shootingDamage = 15;
            ammoCheck();
            DisplayAmmo();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            weaponChoice = "Machine Gun";
            gunChoiceText.text = "Current Weapon: " + weaponChoice;
            shootingDamage = 30;
            ammoCheck();
            DisplayAmmo();
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            weaponChoice = "Shotgun";
            gunChoiceText.text = "Current Weapon: " + weaponChoice;
            shootingDamage = 40;
            ammoCheck();
            DisplayAmmo();
        }
    }

    void OnTriggerStay(Collider obj)
    {


        if (obj.gameObject.tag == "AmmoBox")
        {
            canBuyAmmo = true;
            BroughtAmmo();
        }
    }

    void OnTriggerExit()
    {


        canBuyAmmo = false;
    }

    void BroughtAmmo()
    {

        if (Input.GetKeyDown(KeyCode.F) && canBuyAmmo)
        {
            piSpareAmmo = piMaxAmmo;
            mgSpareAmmo = mgMaxAmmo;
            shSpareAmmo = shMaxAmmo;
            grenadeThrow.canBeShot = true;
            DisplayAmmo();


        }
    }

    void CheckIfMeleeAttacking()
    {
        if (Input.GetKeyDown(KeyCode.Mouse2) && !isMeleeing)
        {
            isMeleeing = true;
            // shoots a ray cast forward, center of position up to the range stated
            if (Physics.Raycast(camTransform.TransformPoint(0, 0, 0), camTransform.forward, out hit, meleeRange))
            {
                // displays in console the tag of the object
                Debug.Log(hit.transform.tag);

                // object check
                if (hit.transform.tag == "Player")
                {
                    // sends identity of player to server
                    string uIdentity = hit.transform.name;
                    CmdFriendlyFire(uIdentity, meleeDamage);

                }
                else if (hit.transform.tag == "Enemy")
                {
                    if (hit.transform)
                    {
                        Debug.Log("Enemy Damaged");
                        string uIdentity = hit.transform.name;
                        CmdEnemyWasShot(uIdentity, meleeDamage);
                    }

                }
            }
            isMeleeing = false;
        }
    }

    void CheckIfShooting()
    {

        //If local player is pressing left mouse button
        if (Input.GetKeyDown(KeyCode.Mouse0) && !isMeleeing && selectedWeaponMag > 0 && weaponChoice == "Pistol")
        {
            recoilAmount += 0.01f;
            weaponADS.AddRecoil(recoilAmount);
            CmdPlayerShoot();
            piMagAmmo -= 1;
            DisplayAmmo();

            //Vector3 pistolFireDirection = camTransform.forward;
            //pistolFireDirection.x += Random.Range(-weaponSpray, weaponSpray);
            //pistolFireDirection.y += Random.Range(-weaponSpray, weaponSpray);
            //pistolFireDirection.z += Random.Range(-weaponSpray, weaponSpray);


            // shoots a ray cast forward, center of position up to the range stated
            if (Physics.Raycast(bulletSpawn.position, camTransform.forward, out hit, shootingRange, layerMask))
            {
                // displays in console the tag of the object
                Debug.Log(hit.transform.tag);

                var hitRotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
                Instantiate(bulletHole, hit.point, hitRotation);


                // object check
                if (hit.transform.tag == "Player")
                {
                    // sends identity of player to server
                    string uIdentity = hit.transform.name;
                    CmdFriendlyFire(uIdentity, shootingDamage);

                }
                if (hit.transform.tag == "Enemy")
                {
                    string uIdentity = hit.transform.name;
                    CmdEnemyWasShot(uIdentity, shootingDamage);
                }
                if (hit.transform.tag == "Crowd")
                {
                    Debug.Log("Crowd Hit");
                    crowdApprovalSystem.shootCrowd = true;
                }
            }
        }

        // If local player is pressing left mouse button
        if (Input.GetKey(KeyCode.Mouse0) && selectedWeaponMag > 0 && weaponChoice == "Machine Gun" && Time.time > mgFireRate + mgLastShot)
        {
            recoilAmount += 0.01f;
            weaponADS.AddRecoil(recoilAmount);
            CmdPlayerShoot();
            mgMagAmmo -= 1;
            DisplayAmmo();
            mgLastShot = Time.time;

            //Vector3 machineGunFireDirection = camTransform.forward;
            //machineGunFireDirection.x += Random.Range(-weaponSpray, weaponSpray);
            //machineGunFireDirection.y += Random.Range(-weaponSpray, weaponSpray);
            //machineGunFireDirection.z += Random.Range(-weaponSpray, weaponSpray);

            // shoots a ray cast forward, center of position up to the range stated
            if (Physics.Raycast(bulletSpawn.position, camTransform.forward, out hit, shootingRange, layerMask))
            {
                
                // displays in console the tag of the object
                Debug.Log(hit.transform.tag);
                //weaponADS.StartRecoil(0.2f, 10f, 10f);

                var hitRotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
                Instantiate(bulletHole, hit.point, hitRotation);

                // object check
                if (hit.transform.tag == "Player")
                {
                    // sends identity of player to server
                    string uIdentity = hit.transform.name;
                    CmdFriendlyFire(uIdentity, shootingDamage);

                }
                if (hit.transform.tag == "Enemy")
                {
                    string uIdentity = hit.transform.name;
                    CmdEnemyWasShot(uIdentity, shootingDamage);
                }
                if (hit.transform.tag == "Crowd")
                {
                    Debug.Log("Crowd Hit");
                    crowdApprovalSystem.shootCrowd = true;
                }

            }
        }

        // If local player is pressing left mouse button
        if (Input.GetKeyDown(KeyCode.Mouse0) && selectedWeaponMag > 0 && weaponChoice == "Shotgun" && Time.time > shFireRate + shLastShot)
        {
            recoilAmount += 0.01f;
            weaponADS.AddRecoil(recoilAmount);
            CmdPlayerShoot();
            shMagAmmo -= 1;
            DisplayAmmo();
            shLastShot = Time.time;

            //Vector3 shotgunFireDirection = camTransform.forward;
            //shotgunFireDirection.x += Random.Range(-weaponSpray, weaponSpray);
            //shotgunFireDirection.y += Random.Range(-weaponSpray, weaponSpray);
            //shotgunFireDirection.z += Random.Range(-weaponSpray, weaponSpray);


            // shoots a ray cast forward, center of position up to the range stated
            if (Physics.Raycast(bulletSpawn.position, camTransform.forward, out hit, shootingRange, layerMask))
            {
                // displays in console the tag of the object
                Debug.Log(hit.transform.tag);

                var hitRotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
                Instantiate(bulletHole, hit.point, hitRotation);

                // object check
                if (hit.transform.tag == "Player")
                {
                    // sends identity of player to server
                    string uIdentity = hit.transform.name;
                    CmdFriendlyFire(uIdentity, shootingDamage);
                }

                if (hit.transform.tag == "Enemy")
                {
                    string uIdentity = hit.transform.name;
                    CmdEnemyWasShot(uIdentity, shootingDamage);
                }

                if (hit.transform.tag == "Crowd")
                {
                    Debug.Log("Crowd Hit");
                    crowdApprovalSystem.shootCrowd = true;
                }
            }
        }
    }

    void CmdPlayerShoot()
    {
        if (weaponChoice == "Pistol")
        {

            var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 300;

            NetworkServer.Spawn(bullet);

            Destroy(bullet, 2.0f);
        }

        if (weaponChoice == "Machine Gun")
        {
            var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 300;

            NetworkServer.Spawn(bullet);

            Destroy(bullet, 2.0f);
        }

        if (weaponChoice == "Shotgun")
        {

            var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 300;

            NetworkServer.Spawn(bullet);

            Destroy(bullet, 2.0f);
        }
    }

    [Command]
    void CmdFriendlyFire(string uniqueID, int dmg)
    {
        // deducts health of the player who has been hit by the ray cast
        GameObject go = GameObject.Find(uniqueID);
        go.GetComponent<PlayerHeath>().DeductHealth(dmg);

    }

    [Command]
    void CmdEnemyWasShot(string uniqueID, int dmg)
    {
        GameObject go = GameObject.Find(uniqueID);
        go.GetComponent<MeleeEnemyHealth>().DeductHealth(dmg);

    }
}