﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerDeath : NetworkBehaviour {

	private PlayerHeath playerHealth;
	private Image crossHair;


	// Use this for initialization
	void Start () 
	{
		crossHair = GameObject.Find ("Image").GetComponent<Image> ();
		playerHealth = GetComponent<PlayerHeath> ();
		playerHealth.EventDie += DisablePlayer;
	}


	void onDisable()
	{
		playerHealth.EventDie -= DisablePlayer;	
	}

	void DisablePlayer()
	{
		GetComponent<CharacterController> ().enabled = false;
		GetComponent<PlayerShooting> ().enabled = false;
		GetComponent<CapsuleCollider> ().enabled = false;

		Renderer[] renderers = GetComponentsInChildren<Renderer> ();
		foreach (Renderer ren in renderers) 
		{
			ren.enabled = false;
		}

		playerHealth.isDead = true;

		if (isLocalPlayer) 
		{
			GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().enabled = false;
			crossHair.enabled = false;
            transform.gameObject.tag = "Downed";
		}
	}
}
