﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerInRangeCheck : NetworkBehaviour {

    private MeleeEnemyMovement enemyMovement;

	// Use this for initialization
	void Start () 
    {
        enemyMovement = gameObject.transform.parent.GetComponent<MeleeEnemyMovement>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	   
	}

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Player")
        {
            enemyMovement.canSeePlayer = true;
        }
    }

    void OnTriggerExit(Collider obj)
    {
        if (obj.gameObject.tag == "Player")
        {
            enemyMovement.canSeePlayer = false;
        }
    }
        
}