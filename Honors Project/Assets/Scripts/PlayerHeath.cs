﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI; 

public class PlayerHeath : NetworkBehaviour {

	[SyncVar (hook = "OnHealthChanged")] private int health = 100;
	private Text healthText;
	private bool shouldDie = false;
	public bool isDead = false;
    //public bool teammateInRange;

	public delegate void DieDelegate ();
	public event DieDelegate EventDie;

	public delegate void RespawnDelegate();
	public event RespawnDelegate EventRespawn;

	// Use this for initialization
    public override void OnStartLocalPlayer()
	{
		healthText = GameObject.Find("Health Text").GetComponent<Text>();
		SetHealthText();
	}

	// Update is called once per frame
	void Update () 
	{
		CheckCondition ();
	}

	void CheckCondition()
	{
		if (health <= 0 && !shouldDie && !isDead) 
		{
			shouldDie = true;
		}

		if(health <= 0 && shouldDie)
		{
			if (EventDie != null) 
			{
				EventDie ();
			}

			shouldDie = false;
		}

		if (health > 0 && isDead) 
		{
			if (EventRespawn != null) 
			{
				EventRespawn ();
			}

			isDead = false;
            transform.gameObject.tag = "Player";
		}
	}

    void OnTriggerStay(Collider obj)
    {
        if (!isLocalPlayer)
        {
            return;
        }

        else if(isLocalPlayer)

        {
            // If Object tag is "Player"
            if (obj.gameObject.tag == "Downed")
            {
                //teammateInRange = true;
                Debug.Log("I have touched the player");
            }
        }
    }

    // If collides with rigidbody then...
    void OnTriggerExit(Collider obj)
    {

        if (!isLocalPlayer)
        {
            return;
        }

        else if (isLocalPlayer)
        {
            if (obj.gameObject.tag == "Downed")
            {
                //teammateInRange = false;
                Debug.Log("Im not touching the player");
            }
        }
    }

   // void RevivePlayer()
   // {
     //   if (teammateInRange)
     //   { 
                    
      //  }
   // }

    void SetHealthText()
    {
        if (isLocalPlayer)
        {
            healthText.text = "Health " + health.ToString();
        }
    }

    public void DeductHealth(int dmg)
    {
        health -= dmg;
    }

    void OnHealthChanged(int hlth)
    {
        health = hlth;
        SetHealthText();
    }

    public void ResetHealth() //Called Player_Respawn
    {
        health = 100;
    }
}
