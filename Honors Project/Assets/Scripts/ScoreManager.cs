﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ScoreManager : NetworkBehaviour 
{
    private Text scoreText;
    [SyncVar (hook = "OnScoreChanged")] public int score = 0;
    MeleeEnemyAttack meleeEnemyHealth;

	// Use this for initialization
	void Start () 
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        SetScoreText();
	}
	
	// Update is called once per frame
	void Update () 
    {
       
	}

    public void AddPoints(int scorePoints)
    {
        score += scorePoints;
    }

    void OnScoreChanged(int points)
    {
        score = points;
        SetScoreText();
    }

    void SetScoreText()
    {
        if (isLocalPlayer)
        {
            scoreText.text = "Score: " + score.ToString();
        }
    }
}