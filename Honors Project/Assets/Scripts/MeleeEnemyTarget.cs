﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class MeleeEnemyTarget : NetworkBehaviour {

	public List<Transform> targets; // Creates a list for the targets transform to go into
	public Transform selectedTarget; // The targeted target transform is stored here
	private Transform myTransform; // Enemies transform is stored here
	NavMeshAgent nav; // calling NavMeshAgent
    public MeleeEnemyMovement enemyMovement;
    public GameObject[] go;
    public Vector3 playerPosition;
    private int sizeOfTargetList;

	void Awake ()
	{
		// Set up the references.
		nav = GetComponent <NavMeshAgent> ();
	}

	// initialise's them
	void Start () 
	{
		targets = new List<Transform>();
		selectedTarget = null;
		myTransform = transform;
        AddAllPlayers();
        enemyMovement = GetComponent<MeleeEnemyMovement>();
	}

	// Add players to array
	public void AddAllPlayers()
	{	
		// Find all game objects tagged called "Player" and store in array
		
       go = GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in go)
			AddPlayer(player.transform);
	}

    public void AddPlayer(Transform player)
    {
        targets.Add(player);
    }

	private void SortTargetByDistance()
	{
        sizeOfTargetList = targets.Count;

        if (sizeOfTargetList > 0)
        {
            // Sorts the target from the lowest distance first
            targets.Sort(delegate(Transform t1, Transform t2)
            {
                return Vector3.Distance(t1.position, myTransform.position).CompareTo(Vector3.Distance(t2.position, myTransform.position));
            });
        }
	}

	// Runs the distance system, finds the closest player
	private void TargetEnemy()
	{
		if (!isServer) 
		{
			return;
		}

		if (selectedTarget == null) 
		{
			SortTargetByDistance ();
			selectedTarget = targets [0];
		} 
			
		else 
			
		{
			int index = targets.IndexOf (selectedTarget);
		}
	}

    public void Update()
    {
        SortTargetByDistance();
        TargetEnemy();
    }

}