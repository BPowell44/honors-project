﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerNetworkRotation : NetworkBehaviour {

	[SyncVar] private Quaternion playerRotation;
	[SyncVar] private Quaternion cameraRotation;

	[SerializeField] private Transform playerTransform;
	[SerializeField] private Transform cameraTransform;
	[SerializeField] private float lerpRate = 15;

	private Quaternion lastPlayerRotation;
	private Quaternion lastCameraRotation;
	private float threshold = 5;


	void Update () 
	{
		LerpRotations ();
	}

	void FixedUpdate () 
	{
		TransmitRotations ();
		//LerpPosition ();
	}
		
	void LerpRotations ()
	{
		if (!isLocalPlayer) 
		{
			playerTransform.rotation = Quaternion.Lerp (playerTransform.rotation, playerRotation, Time.deltaTime * lerpRate);
			cameraTransform.rotation = Quaternion.Lerp (cameraTransform.rotation, cameraRotation, Time.deltaTime * lerpRate);
		}
	}

	[Command]
	void CmdProvideRotationsToServer (Quaternion playerRot, Quaternion camRot)
	{
		playerRotation = playerRot;
		cameraRotation = camRot;
	}

	[Client]
	void TransmitRotations ()
	{
		if (isLocalPlayer) 
		{
			if (Quaternion.Angle (playerTransform.rotation, lastPlayerRotation) > threshold || Quaternion.Angle (cameraTransform.rotation, lastCameraRotation) > threshold)
			{
				CmdProvideRotationsToServer (playerTransform.rotation, cameraTransform.rotation);
				lastPlayerRotation = playerTransform.rotation;
				lastCameraRotation = cameraTransform.rotation;
			}
		}
	}
}
