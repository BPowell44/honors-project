﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI; 

public class EnemySpawnScript : NetworkBehaviour
{
    private int counter;
    public GameObject normalEnemy;
    public GameObject fastEnemy;
    public GameObject bruteEnemy;
    public GameObject shootingEnemy;
    public GameObject bossEnemy;

    private float normalEnemyTimer;
    private float fastEnemyTimer;
    private float bruteEnemyTimer;
    private float shootingEnemyTimer;
    private float bossEnemyTimer;

    public int killCount;
    public int enemiesLeft;
    private bool startRound = false;
    public bool inBetweenRounds = false;
    public float roundTimer;
    public float breakTimer;
    public float GUITimer;
    public int roundCounter = 0;

    float Range; // Used to find a random range
    public GameObject[] spawnPoints;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        WaveManager();
        roundTimer += Time.deltaTime;
        GameObject.Find("EnemyLeft Text").GetComponent<Text>().text = "Enemies Left: " + enemiesLeft.ToString();
    }
    
    void WaveManager()
    {
            if (killCount == 0 && !startRound) // Round 1
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 1;
                RoundGUIIController();


                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    enemiesLeft = 15;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(10, 5, 0, 0, 0);
                    startRound = true;

                }
            }

            else if (killCount == 15 && startRound) // Round 2
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 2;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    enemiesLeft = 30;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(15, 10, 5, 0, 0);
                    startRound = false;
                }
            }

            else if (killCount == 45 && !startRound) //Round 3
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 3;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    enemiesLeft = 21;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(10, 5, 3, 2, 1);
                    startRound = true;   
                }
            }

            else if (killCount == 66 && startRound) // Round 4
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 4;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(0, 20, 6, 5, 0);
                    startRound = false;
                    enemiesLeft = 31;
                }
            }

            else if (killCount == 97 && !startRound) // Round 5
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 5;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(10, 5, 10, 10, 0);
                    startRound = true;
                    enemiesLeft = 35;
                }
            }

            else if (killCount == 132 && startRound) // Round 6
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 6;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(20, 15, 5, 10, 0);
                    startRound = false;
                    enemiesLeft = 50;
                }
            }

            else if (killCount == 182 && !startRound) // Round 7
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 7;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(15, 10, 4, 2, 1);
                    startRound = true;
                    enemiesLeft = 32;
                }
                
            }

            else if (killCount == 214 && startRound) // Round 8
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 8;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(15, 25, 6, 8, 0);
                    startRound = false;
                    enemiesLeft = 54;
                }
            }

            else if (killCount == 268 && !startRound) // Round 9
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 9;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(25, 20, 15, 5, 0);
                    startRound = true;
                    enemiesLeft = 65;
                }
            }

            else if (killCount == 333 && startRound) // Round 10
            {
                inBetweenRounds = true;
                breakTimer += Time.deltaTime;
                GUITimer += Time.deltaTime;
                roundCounter = 10;
                RoundGUIIController();

                if (breakTimer > 5)
                {
                    inBetweenRounds = false;
                    breakTimer = 0;
                    roundTimer = 0;
                    GameObject.Find("Round Text").GetComponent<Text>().text = "Wave: " + roundCounter;
                    EnemyWaveSpawning(30, 30, 10, 15, 1);
                    startRound = false;
                    enemiesLeft = 86;
                }
            }
    }

    void RoundGUIIController()
    { 
        //GameObject.Find("RoundInformation").GetComponent<Text>().enabled = true;

        if (GUITimer > 3 && inBetweenRounds)
        {
            GameObject.Find("RoundInformation").GetComponent<Text>().text = "Wave " + roundCounter + " About To Begin! \n Get Ready!";
            GameObject.Find("RoundInformation").GetComponent<Text>().enabled = true;

            if (GUITimer > 5 && inBetweenRounds)
            {
                GameObject.Find("RoundInformation").GetComponent<Text>().enabled = false;
                GUITimer = 0;
            }
        }
    }

    private void EnemyWaveSpawning(int p_normalEnemyNo, int p_fastEnemyNo, int p_bruteEnemyNo, int p_shootingEnemyNo, int p_bossEnemyNo)
    {
        for (int i = 0; i < p_normalEnemyNo; i++)
        {
            spawnNormalEnemy();
        }

        for (int i = 0; i < p_fastEnemyNo; i++)
        {
            spawnFastEnemy();
        }

        for (int i = 0; i < p_bruteEnemyNo; i++)
        {

                spawnBruteEnemy();
        }

        for (int i = 0; i < p_shootingEnemyNo; i++)
        {
                spawnShootingEnemy();
        }

        for (int i = 0; i < p_bossEnemyNo; i++)
        {
                spawnBossEnemy();
        }
    
    }

    void spawnNormalEnemy()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        counter++;
        // Create an instance of the enemy prefab at the randomly selected spawn point's position
        GameObject ne = Instantiate(normalEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(ne);
        ne.GetComponent<EnemyID>().enemyID = "Enemy " + counter;
    }

    void spawnFastEnemy()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        counter++;
        // Create an instance of the enemy prefab at the randomly selected spawn point's position
        GameObject fe = Instantiate(fastEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(fe);
        fe.GetComponent<EnemyID>().enemyID = "Enemy " + counter;
    }

    void spawnBruteEnemy()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        counter++;
        // Create an instance of the enemy prefab at the randomly selected spawn point's position
        GameObject be = Instantiate(bruteEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(be);
        be.GetComponent<EnemyID>().enemyID = "Enemy " + counter;
    }

    void spawnShootingEnemy()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        counter++;
        // Create an instance of the enemy prefab at the randomly selected spawn point's position
        GameObject se = Instantiate(shootingEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(se);
        se.GetComponent<EnemyID>().enemyID = "Enemy " + counter;
    }

    void spawnBossEnemy()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        counter++;
        // Create an instance of the enemy prefab at the randomly selected spawn point's position
        GameObject boss = Instantiate(bossEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(boss);
        boss.GetComponent<EnemyID>().enemyID = "Enemy " + counter;
    }
}
