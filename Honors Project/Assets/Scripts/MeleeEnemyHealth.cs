﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MeleeEnemyHealth : NetworkBehaviour
{
    public EnemySpawnScript enemySpawnScript;
    public CrowdApprovalSystem crowdApproval;
    public MeleeEnemyMovement meleeEnemyMovement;
	public int enemyHealth = 100;
    public int hitValue = 25;
    public int killValue = 100;
    //ScoreManager scoreManager;
    public bool isDead;
    private float timer;
    Animator anim;


    void Start()
    {
        anim = GetComponent<Animator>();
        meleeEnemyMovement = GetComponent<MeleeEnemyMovement>();
        enemySpawnScript = GameObject.Find("EnemySpawnManager").GetComponent<EnemySpawnScript>();
        crowdApproval = GameObject.Find("GameManager").GetComponent<CrowdApprovalSystem>();
        //scoreManager = GetComponent<ScoreManager>();
    }
	public void DeductHealth (int dmg)
	{
		
		enemyHealth -= dmg;
        //SendPoints(hitValue);

		if (enemyHealth <= 0) 
		{
            //SendPoints(killValue);
            GetComponent<NavMeshAgent>().enabled = false;
			Death ();
		}
	}

    void Update()
    {
        timer += Time.deltaTime;
    }

    void Death()
    {
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        anim.SetTrigger("Dead");
        enemySpawnScript.killCount++;
        enemySpawnScript.enemiesLeft--;
        crowdApproval.enemyKilled = true;


        Destroy(gameObject, 5);

    }

    //public void SendPoints(int score)
    //{
    //    scoreManager.GetComponent<ScoreManager>().AddPoints(score);
    //}  
}