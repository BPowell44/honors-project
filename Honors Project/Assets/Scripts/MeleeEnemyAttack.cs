﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MeleeEnemyAttack : NetworkBehaviour {

	public float timeBetweenAttacks = 2.0f;
	public int attackDamage = 10;
	PlayerHeath playerHealth;
	bool playerInRange;
	float timer;
    private MeleeEnemyTarget enemyTarget;
	Animator anim;
    private float animTimer;

    void Start()
    {
		anim = GetComponent<Animator> ();
        enemyTarget = GetComponent<MeleeEnemyTarget>();
        GameObject.FindGameObjectsWithTag("Player");
    }

	// If collides with rigidbody then...
	void OnTriggerEnter(Collider obj) 
	{	
		// If Object tag is "Player"
		if (obj.gameObject.tag == "Player")
		{
			playerInRange = true;
		}
	}

	// If collides with rigidbody then...
    void OnTriggerExit(Collider obj) 
	{
        if (obj.gameObject.tag == "Player")
        {
            playerInRange = false;
        }
	}

    void Update()
    {
        //if (isServer)
        //{
            // Add the time since last called
            timer += Time.deltaTime;
            animTimer += Time.deltaTime;

            // If the timer exceeds the time between attacks, the player is in range and this enemy is alive
            if (timer >= timeBetweenAttacks && playerInRange)
            {
                AttackPlayer();
            }
       // }
    }

	void AttackPlayer ()
	{
		// Restarts the timer
		timer = 0f;

        anim.SetTrigger("Attack");
        
			// If the player has health to lose
			enemyTarget.selectedTarget.GetComponent<PlayerHeath> ().DeductHealth (attackDamage);
            //anim.SetBool("isAttacking", false);
	}
}
