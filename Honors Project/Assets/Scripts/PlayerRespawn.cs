﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerRespawn : NetworkBehaviour {

	// references and variables
	private PlayerHeath playerHealth;
	private Image crossHair;

	// Use this for initialization
	void Start () 
	{
		// when program starts, find this components
		playerHealth = GetComponent<PlayerHeath> ();
		playerHealth.EventRespawn += EnablePlayer;
		crossHair = GameObject.Find("Image").GetComponent<Image>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		PlayerDeadCheck ();
	}

	void OnDisable()
	{
		playerHealth.EventRespawn -= EnablePlayer;
	}

	void EnablePlayer ()
	{
		// turning on components
		GetComponent<CharacterController> ().enabled = true;
		GetComponent<PlayerShooting> ().enabled = true;
		GetComponent<CapsuleCollider> ().enabled = true;

		// turns on renderers so players can see each other
		Renderer[] renderers = GetComponentsInChildren<Renderer> ();
		foreach (Renderer ren in renderers) 
		{
			ren.enabled = true;
		}
			
		// turns on player controller and crosshair
		if (isLocalPlayer) 
		{
			GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().enabled = true;
			crossHair.enabled = true;

		}
	}

	// Need to finish
	void PlayerDeadCheck()
	{
		if (isLocalPlayer) 
		{
			if (playerHealth.isDead == true) 
			{
				if (Input.GetKeyDown(KeyCode.Space)) 
				{
					//EnablePlayer ();
					CommenceRespawn ();
				}
					
			}
				
		}
	}

	// Need to finish
	void CommenceRespawn()
	{
		CmdRespawnOnServer ();
	}

	// Need to finish
	[Command]
	void CmdRespawnOnServer()
	{
		playerHealth.ResetHealth ();
	}
}
