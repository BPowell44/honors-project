﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    private Transform playerTransform;
    private float characterHeight;
    private Vector3 characterPosition;
    private CharacterController characterController;

    // Use this for initialization
    void Start()
    {
        playerTransform = transform;
        characterController = GetComponent<CharacterController>();
        characterHeight = characterController.height;
    }

    // Update is called once per frame
    void Update()
    {
        Crouch();
    }

    void Crouch()
    {
        float tempHeight = characterHeight;

        if (Input.GetKey(KeyCode.C))
        {
            tempHeight = 0.5f;
        }

        float lastHeight = characterController.height;
        characterController.height = Mathf.MoveTowards(characterController.height, tempHeight, Time.deltaTime * 5);
        characterPosition.x = playerTransform.position.x;
        characterPosition.z = playerTransform.position.z;
        characterPosition.y = playerTransform.position.y + (characterController.height - lastHeight) / 2;
        playerTransform.position = characterPosition;
    }
}