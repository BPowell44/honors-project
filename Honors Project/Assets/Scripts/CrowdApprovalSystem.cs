﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CrowdApprovalSystem : NetworkBehaviour 
{
    private PlayerShooting playerShooting;
    private MeleeEnemyHealth enemyHealth;
    private EnemySpawnScript enemySpawnScript;

    private int multipleEnemiesKilled;
    private int crowdApproval = 50;
    private float crowdTimer;
    private float longRoundTimer = 10f;

    private bool isLongRound = false;
    public bool shootCrowd = false;
    public bool enemyKilled = false;
    //public bool inBetweenRounds = false;



	// Use this for initialization
	void Start () 
    {
        enemySpawnScript = GameObject.Find("EnemySpawnManager").GetComponent<EnemySpawnScript>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        CrowdRating();
        CrowdShot();
	    GameObject.Find("Crowd Approval Text").GetComponent<Text>().text = "Crowd Approval: " + crowdApproval.ToString();
	}

    void CrowdRating()
    {
        if (!enemySpawnScript.inBetweenRounds)
        {
            crowdTimer += Time.deltaTime;

            if (crowdTimer > 2)
            {
                crowdApproval -= 1;
                crowdTimer = 0;
            }

            else if (enemySpawnScript.roundTimer > longRoundTimer && !isLongRound)
            {
                crowdApproval -= 20;
                crowdTimer = 0;
                isLongRound = true;
            }
        }

        if (enemyKilled)
        {
            crowdApproval += 5;
            enemyKilled = false;
        }        
    }

    void CrowdShot()
    {
        if (shootCrowd)
        {
            crowdApproval -= 30;
            shootCrowd = false;
        }
        else
            return;
    }
}