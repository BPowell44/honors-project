﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ShootingEnemyAttack : NetworkBehaviour 
{
    private ShootingEnemyTarget shootingEnemyTarget;
    private int attackDamage = 1;
    private Transform targetTransform;
    PlayerHeath playerHealth;
    public bool playerInRange = false;
    public float range = 200;
    public Transform weaponTransform;
    private RaycastHit hit;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    private Vector3 playerPosition;

    private UnityStandardAssets.Characters.FirstPerson.FirstPersonController playerController;
    private Vector3 missedShotPosition;
    private int randonAcc = 0;
    private int minAcc = 35;
    private int hitValue = 0;
    private Vector3 missShotAtPlayer;

    private float FireRate = 0.7f;
    private float LastShot = 0.0f;

	// Use this for initialization
	void Start () 
    {
        playerController = GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        shootingEnemyTarget = transform.parent.GetComponent<ShootingEnemyTarget>();
        GameObject.FindGameObjectsWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () 
    {
        TargetEnemy();  
	}

    void TargetEnemy()
    {
        targetTransform = transform.parent.GetComponent<ShootingEnemyTarget>().selectedTarget;
    }

    // If collides with rigidbody then...
    void OnTriggerStay(Collider obj)
    {
        // If Object tag is "Player"
        if (obj.gameObject.tag == "Player")
        {
            playerInRange = true;
            Debug.Log("I Can Shoot The Player");
            ChanceToHit();
        }
    }

    // If collides with rigidbody then...
    void OnTriggerExit(Collider obj)
    {
        if (obj.gameObject.tag == "Player")
        {
            playerInRange = false;
            Debug.Log("I Cannot Shoot The Player");
        }
    }

    void ChanceToHit()
    {
        if (playerInRange && Time.time > FireRate + LastShot)
        {
            randonAcc = Random.Range(0,60);
            hitValue = minAcc + randonAcc;

            if (hitValue >= 70)
            {
                ShootTarget();
            }

            else if (hitValue < 70)
            {
                MissTarget();
            }
        }

        hitValue = 0;
        randonAcc = 0;
    }

    void MissTarget()
    {
        missedShotPosition = new Vector3 (Random.Range(-7.0F, 7.0F), Random.Range(-7.0f, 7.0f), Random.Range(-7.0F, 7.0F));
        playerPosition = targetTransform.transform.position - transform.position;
        missedShotPosition += playerPosition;
        CmdFireBulletAndMissPlayer();
        missedShotPosition = new Vector3(0, 0, 0);
    }

    void ShootTarget()
    {
        playerPosition = targetTransform.transform.position - transform.position;

        if (Physics.Raycast(weaponTransform.TransformPoint(0, 0, 0.5f), playerPosition, out hit, range))
        {
            // displays in console the tag of the object
            Debug.Log(hit.transform.tag);

            // object check
            if (hit.transform.tag == "Player")
            {
                // sends identity of player to server
                string uIdentity = hit.transform.name;
                shootingEnemyTarget.selectedTarget.GetComponent<PlayerHeath>().DeductHealth(attackDamage);
                CmdFireBulletAtPlayer();
                }
            }
    }
    

    void CmdFireBulletAtPlayer()
    {
        var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        bullet.GetComponent<Rigidbody>().velocity = playerPosition * 5;

        NetworkServer.Spawn(bullet);

        Destroy(bullet, 2.0f);

        LastShot = Time.time;
    }

    void CmdFireBulletAndMissPlayer()
    {
        var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        bullet.GetComponent<Rigidbody>().velocity = missedShotPosition * 5;

        NetworkServer.Spawn(bullet);

        Destroy(bullet, 2.0f);

        LastShot = Time.time;
    }
}
