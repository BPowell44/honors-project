﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ShootingEnemyRangeCheck : NetworkBehaviour {

    private ShootingEnemyMovement ShootingEnemyMovement;

    // Use this for initialization
    void Start()
    {
        ShootingEnemyMovement = gameObject.transform.parent.GetComponent<ShootingEnemyMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Player")
        {
            ShootingEnemyMovement.canSeePlayer = true;
        }
    }

    void OnTriggerExit(Collider obj)
    {
        if (obj.gameObject.tag == "Player")
        {
            ShootingEnemyMovement.canSeePlayer = false;
        }
    }

}