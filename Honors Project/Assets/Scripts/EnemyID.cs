﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EnemyID : NetworkBehaviour 
{
	[SyncVar] public string enemyID;
	private Transform myTransform;

	void Start ()
	{
		myTransform = transform;
	}

	void Update ()
	{
		SetIdentity ();
	}

	void SetIdentity ()
	{
        if (myTransform.name == "" || myTransform.name == "Ghoul(Clone)")
		{
			myTransform.name = enemyID;
		}

        else if (myTransform.name == "" || myTransform.name == "Swarm(Clone)")
        {
			myTransform.name = enemyID;
		}

        else if (myTransform.name == "" || myTransform.name == "Rankor(Clone)")
        {
            myTransform.name = enemyID;
        }

        else if (myTransform.name == "" || myTransform.name == "AlienShooter(Clone)")
        {
            myTransform.name = enemyID;
        }

        else if (myTransform.name == "" || myTransform.name == "Boss(Clone)")
        {
            myTransform.name = enemyID;
        }
	}
}