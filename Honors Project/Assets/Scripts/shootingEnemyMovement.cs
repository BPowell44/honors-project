﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(NavMeshAgent))]
public class ShootingEnemyMovement : MonoBehaviour
{
    // Reference to NavMeshAgent component
    private NavMeshAgent nav;
    private bool firstWaypoint = true;
    private Vector3 tempEnemyPosition;
    private Vector3 tempWaypointPosition;
    public Transform[] waypoints;
    private int curWaypoint = Random.Range(0, 13);
    public float minWaypointDistance = 2.0f;
    public bool canSeePlayer = false;
    private ShootingEnemyTarget ShootingEnemyTarget;
    [SerializeField]
    private float minShootingDistance = 15.0f;
    private float shootDistanceTimer;
    private Vector3 minDistance;


    private void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        ShootingEnemyTarget = GetComponent<ShootingEnemyTarget>();
        GameObject.FindGameObjectsWithTag("Player");
    }

    // Every frame...
    private void Update()
    {
        Patrolling();
        Chasing();
    }

    public void Patrolling()
    {
        if (!canSeePlayer)
        {
            // Agents position (x, set y to 0, z)
            tempEnemyPosition = transform.position;
            tempEnemyPosition.y = 0;

            // Current waypoints position (x, set y to 0, z)
            tempWaypointPosition = waypoints[curWaypoint].position;
            tempWaypointPosition.y = 0;

            // Is the distance between the agent and the current waypoint within the minWaypointDistance?
            if (Vector3.Distance(tempEnemyPosition, tempWaypointPosition) <= minWaypointDistance)
            {
                curWaypoint = Random.Range(0, 13);
            }

            // Set the destination for the agent
            // The navmesh agent is going to do the rest of the work
            nav.SetDestination(waypoints[curWaypoint].position);
        }
    }

    public void Chasing()
    {
        if (canSeePlayer)
        {
            nav.SetDestination(ShootingEnemyTarget.selectedTarget.position);
        }
    }
}