﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerNetworkSetup : NetworkBehaviour 
{

	//Stores the camera into memory
	[SerializeField] Camera FPSCharacterCam;
	//Stores the Audio Listener into memory
	[SerializeField] AudioListener audioListener;


	public override void OnStartLocalPlayer() 
	{
       
            //Finds the scene camera and turns it off
            GameObject.Find("Scene Camera").SetActive(false);
            //Enables the Character Controller
            GetComponent<CharacterController>().enabled = true;
            //Enables the First Person Controller
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            //Enables the first person camera
            FPSCharacterCam.enabled = true;
            //Enables the audio listner on the player
            audioListener.enabled = true;

	}
}
