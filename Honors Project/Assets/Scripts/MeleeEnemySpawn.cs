﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MeleeEnemySpawn : NetworkBehaviour 
{
    private int counter;
    public GameObject meleeEnemy;	// The enemy prefab to be spawned.
    private float meleeTimePassed = 0f; // Stores value of time passed
    private float meleeSpawnRate = 10f; // The rate the enemy spawn
    private int meleeNumberOfEnemy = 2; // How many enemies spawn
    private float meleeSpawn = 10f; // Used to control the spawn time

    public GameObject fastEnemy;	// The enemy prefab to be spawned.
    private float fastTimePassed = 0f; // Stores value of time passed
    private float fastSpawnRate = 10f; // The rate the enemy spawn
    private int fastNumberOfEnemy = 2; // How many enemies spawn
    private float fastSpawn = 10f; // Used to control the spawn time

    public GameObject bruteEnemy;	// The enemy prefab to be spawned.
    private float bruteTimePassed = 0f; // Stores value of time passed
    private float bruteSpawnRate = 10f; // The rate the enemy spawn
    private int bruteNumberOfEnemy = 2; // How many enemies spawn
    private float bruteSpawn = 10f; // Used to control the spawn time

    public GameObject bossEnemy;	// The enemy prefab to be spawned.
    private float bossTimePassed = 0f; // Stores value of time passed
    private float bossSpawnRate = 10f; // The rate the enemy spawn
    private int bossNumberOfEnemy = 2; // How many enemies spawn
    private float bossSpawn = 10f; // Used to control the spawn time

    public GameObject shootingEnemy;	// The enemy prefab to be spawned.
    private float shootingTimePassed = 0f; // Stores value of time passed
    private float shootingSpawnRate = 10f; // The rate the enemy spawn
    private int shootingNumberOfEnemy = 2; // How many enemies spawn
    private float shootingSpawn = 10f; // Used to control the spawn time

    float Range; // Used to find a random range
    public GameObject[] spawnPoints;	// An array of the spawn points this enemy can spawn from

    public override void OnStartServer ()
    {
        // Spawn time depends on time from first frame and value of 'spawn timer
        shootingSpawnRate = Time.time + shootingSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        spawnMeleeEnemy();
        spawnFastEnemy();
        spawnBruteEnemy();
        spawnBossEnemy();
        spawnShootingEnemy();
    }

    void spawnMeleeEnemy()
    {
        // Store the time gone in variable
        meleeTimePassed += Time.deltaTime;

        // After 10 seconds
        if (meleeTimePassed >= 20f)
        {
            // Minus '0.5' of spawn rate if 'm' isnt '1'
            if (meleeSpawn >= 15)
            {
                meleeSpawn -= 0.5f;
            }

            // Spawn 'x' amount of enemies
            meleeNumberOfEnemy += 1;
            // Time vaule reset
            meleeTimePassed = 0f;
        }

        // Controls the spawnrate, without it, it would just keep spawning
        if (meleeSpawnRate < Time.time)
        {
            // Loop is used to initiate spawn
            for (int i = 0; i < meleeNumberOfEnemy; i++)
            {
                // Find a random index between zero and one less than the number of spawn points.
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);

                counter++;
                // Create an instance of the enemy prefab at the randomly selected spawn point's position
                GameObject me = Instantiate(meleeEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(me);
                me.GetComponent<EnemyID>().enemyID = "Enemy " + counter;

            }

            meleeSpawnRate = Time.time + meleeSpawn;
        }
    }

    void spawnFastEnemy()
    {
        // Store the time gone in variable
        fastTimePassed += Time.deltaTime;

        // After 10 seconds
        if (fastTimePassed >= 20f)
        {
            // Minus '0.5' of spawn rate if 'm' isnt '1'
            if (fastSpawn >= 15)
            {
                fastSpawn -= 0.5f;
            }

            // Spawn 'x' amount of enemies
            fastNumberOfEnemy += 1;
            // Time vaule reset
            fastTimePassed = 0f;
        }

        // Controls the spawnrate, without it, it would just keep spawning
        if (fastSpawnRate < Time.time)
        {
            // Loop is used to initiate spawn
            for (int i = 0; i < fastNumberOfEnemy; i++)
            {
                // Find a random index between zero and one less than the number of spawn points.
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);

                counter++;
                // Create an instance of the enemy prefab at the randomly selected spawn point's position
                GameObject fe = Instantiate(fastEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(fe);
                fe.GetComponent<EnemyID>().enemyID = "Enemy " + counter;

            }

            fastSpawnRate = Time.time + fastSpawn;
        }
    }

    void spawnBruteEnemy()
    {
        // Store the time gone in variable
        bruteTimePassed += Time.deltaTime;

        // After 10 seconds
        if (bruteTimePassed >= 20f)
        {
            // Minus '0.5' of spawn rate if 'm' isnt '1'
            if (bruteSpawn >= 15)
            {
                bruteSpawn -= 0.5f;
            }

            // Spawn 'x' amount of enemies
            bruteNumberOfEnemy += 1;
            // Time vaule reset
            bruteTimePassed = 0f;
        }

        // Controls the spawnrate, without it, it would just keep spawning
        if (bruteSpawnRate < Time.time)
        {
            // Loop is used to initiate spawn
            for (int i = 0; i < bruteNumberOfEnemy; i++)
            {
                // Find a random index between zero and one less than the number of spawn points.
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);

                counter++;
                // Create an instance of the enemy prefab at the randomly selected spawn point's position
                GameObject be = Instantiate(bruteEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(be);
                be.GetComponent<EnemyID>().enemyID = "Enemy " + counter;

            }

            bruteSpawnRate = Time.time + bruteSpawn;
        }
    }

    void spawnBossEnemy()
    {
        // Store the time gone in variable
        bossTimePassed += Time.deltaTime;

        // After 10 seconds
        if (bossTimePassed >= 20f)
        {
            // Minus '0.5' of spawn rate if 'm' isnt '1'
            if (bossSpawn >= 15)
            {
                bossSpawn -= 0.5f;
            }

            // Spawn 'x' amount of enemies
            bossNumberOfEnemy += 1;
            // Time vaule reset
            bossTimePassed = 0f;
        }

        // Controls the spawnrate, without it, it would just keep spawning
        if (bossSpawnRate < Time.time)
        {
            // Loop is used to initiate spawn
            for (int i = 0; i < bossNumberOfEnemy; i++)
            {
                // Find a random index between zero and one less than the number of spawn points.
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);

                counter++;
                // Create an instance of the enemy prefab at the randomly selected spawn point's position
                GameObject bo = Instantiate(bossEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(bo);
                bo.GetComponent<EnemyID>().enemyID = "Enemy " + counter;

            }

            bossSpawnRate = Time.time + bossSpawn;
        }
    }

    void spawnShootingEnemy()
    {
        // Store the time gone in variable
        shootingTimePassed += Time.deltaTime;

        // After 10 seconds
        if (shootingTimePassed >= 20f)
        {
            // Minus '0.5' of spawn rate if 'm' isnt '1'
            if (shootingSpawn >= 15)
            {
                shootingSpawn -= 0.5f;
            }

            // Spawn 'x' amount of enemies
            shootingNumberOfEnemy += 1;
            // Time vaule reset
            shootingTimePassed = 0f;
        }

        // Controls the spawnrate, without it, it would just keep spawning
        if (shootingSpawnRate < Time.time)
        {
            // Loop is used to initiate spawn
            for (int i = 0; i < shootingNumberOfEnemy; i++)
            {
                // Find a random index between zero and one less than the number of spawn points.
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);

                counter++;
                // Create an instance of the enemy prefab at the randomly selected spawn point's position
                GameObject se = Instantiate(bossEnemy, spawnPoints[spawnPointIndex].transform.position, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(se);
                se.GetComponent<EnemyID>().enemyID = "Enemy " + counter;

            }

            shootingSpawnRate = Time.time + shootingSpawn;
        }
    }

}