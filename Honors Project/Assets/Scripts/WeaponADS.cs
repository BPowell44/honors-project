﻿using UnityEngine;
using System.Collections;

public class WeaponADS : MonoBehaviour {

    public float defaultFOV = 60.0F;
    public float aimedFOV = 45.0F;
    public float smoothFOV = 10.0F;
    public Vector3 hipPosition;
    public Vector3 aimPosition;
    public float smoothAim = 12F;
    public bool aimingDownSight;

    public float leftleanAngle = 45.0f;
	public float rightleanAngle = -45.0f;
    public Vector3 tempPosition;
    public float angle;
    private float peakSpeed = 50f;

    public float recoil = 0;
    public float maxRecoilX = -5;//Play Around with these Variables
    public float maxRecoilY = 5;
    public float recoilSpeed = 2;

    public Transform playerCamera;
    public bool isPeaking = false;

    private PlayerShooting playerShooting;

    void Start()
    {
        playerShooting = transform.parent.parent.GetComponent<PlayerShooting>();
    }

    void Update()
    {
        AimDownSight();
        Peaking();
        //SetRecoil();
    }

    public void AimDownSight()
    {
		if (Input.GetMouseButton(1))
        {
            aimingDownSight = true;
            transform.localPosition = Vector3.Lerp(transform.localPosition, aimPosition, Time.deltaTime * smoothAim);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, aimedFOV, Time.deltaTime * smoothFOV);
            //playerShooting.isAimingDownSight = true;
        }
        else
        {
            aimingDownSight = false;
            transform.localPosition = Vector3.Lerp(transform.localPosition, hipPosition, Time.deltaTime * smoothAim);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, defaultFOV, Time.deltaTime * smoothFOV);
            //playerShooting.isAimingDownSight = true;
        }
    }

    public void Peaking()
    {
        if (aimingDownSight)
        {

            if (Input.GetKey(KeyCode.Q))
            {
				playerCamera.localRotation = Quaternion.Slerp(playerCamera.localRotation, Quaternion.Euler(0,0,80), Time.deltaTime * smoothAim);
            }

            if (Input.GetKey(KeyCode.E))
            {
				playerCamera.localRotation = Quaternion.Slerp(playerCamera.localRotation, Quaternion.Euler(0,0,-30), Time.deltaTime * smoothAim);
        	}
            else 
            {
				playerCamera.localRotation = Quaternion.Slerp(playerCamera.localRotation, Quaternion.Euler(0,0,0), Time.deltaTime * smoothAim);
            }
            
        }
        else 
        {
			playerCamera.localRotation = Quaternion.Slerp(playerCamera.localRotation, Quaternion.Euler(0,0,0), Time.deltaTime * smoothAim);
        }
    }

    public void AddRecoil(float recoilNumber)
    {
        recoil += recoilNumber;
        Debug.Log(recoil + "first");
    }

    //void SetRecoil()
    //{
    //    if (recoil > 0)
    //    {
    //        Debug.Log(recoil + "second");

    //        var maxRecoil = Quaternion.Euler( 0, maxRecoilX + Random.Range(-maxRecoilX, maxRecoilX), maxRecoilY + Random.Range(-maxRecoilY, maxRecoilY));
    //        playerCamera.rotation = Quaternion.Slerp(playerCamera.localRotation, maxRecoil, Time.deltaTime * recoilSpeed);
    //        playerCamera.localEulerAngles = new Vector3(playerCamera.localEulerAngles.x, playerCamera.localEulerAngles.y, playerCamera.localEulerAngles.z);
    //        recoil -= Time.deltaTime;


    //        //Quaternion maxRecoil = Quaternion.Euler(maxRecoilX, maxRecoilY, 0);
    //        //playerCamera.localRotation = Quaternion.Slerp(playerCamera.localRotation, maxRecoil, Time.deltaTime * recoilSpeed);
    //        //recoil -= Time.deltaTime;
    //    }
    //    else
    //    {
    //        recoil = 0;
    //        var minRecoil = Quaternion.Euler(0, 0, 0);
    //        playerCamera.localRotation = Quaternion.Slerp(playerCamera.localRotation, Quaternion.identity, Time.deltaTime * recoilSpeed);
    //        playerCamera.localEulerAngles = new Vector3(playerCamera.localEulerAngles.x, playerCamera.localEulerAngles.y, playerCamera.localEulerAngles.z);
    //    }
    //}
}