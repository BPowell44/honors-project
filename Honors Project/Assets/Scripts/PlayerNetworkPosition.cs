﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerNetworkPosition : NetworkBehaviour {

	[SyncVar] 
	private Vector3 syncPos;

	[SerializeField] Transform playerTransform;
	[SerializeField] float lerpRate = 15;

	private Vector3 lastPostion;
	private float threshold = 0.5f;

	void Update () 
	{
		LerpPosition ();
	}

	void FixedUpdate () 
	{
		TransmitPosition ();
		//LerpPosition ();
	}

	void LerpPosition ()
	{
		if (!isLocalPlayer) {
			playerTransform.position = Vector3.Lerp (playerTransform.position, syncPos, Time.deltaTime * lerpRate);
		}
	}

	[Command]
	void CmdProvidePositionToServer (Vector3 pos)
	{
		syncPos = pos;
	}

	[ClientCallback]
	void TransmitPosition ()
	{
		if (isLocalPlayer && Vector3.Distance(playerTransform.position, lastPostion) > threshold)
		{
			CmdProvidePositionToServer (playerTransform.position);
			lastPostion = playerTransform.position;
		}
	}
}
