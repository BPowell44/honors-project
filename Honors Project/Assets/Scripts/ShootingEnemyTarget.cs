﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ShootingEnemyTarget : NetworkBehaviour
{

    public List<Transform> targets; // Creates a list for the targets transform to go into
    public Transform selectedTarget; // The targeted target transform is stored here
    private Transform myTransform; // Enemies transform is stored here
    NavMeshAgent nav; // calling NavMeshAgent
    public ShootingEnemyMovement shootingEnemyMovement;
    public GameObject[] go;

    void Awake()
    {
        // Set up the references.
        nav = GetComponent<NavMeshAgent>();
    }

    // initialise's them
    void Start()
    {
        targets = new List<Transform>();
        selectedTarget = null;
        myTransform = transform;
        AddAllPlayers();
        shootingEnemyMovement = GetComponent<ShootingEnemyMovement>();
    }

    // Add players to array
    public void AddAllPlayers()
    {
        // Find all game objects tagged called "Player" and store in array

        go = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in go)
            AddPlayer(player.transform);
    }

    public void AddPlayer(Transform player)
    {
        targets.Add(player);
    }

    private void SortTargetByDistance()
    {	// Sorts the target from the lowest distance first
        targets.Sort(delegate(Transform t1, Transform t2)
        {
            return Vector3.Distance(t1.position, myTransform.position).CompareTo(Vector3.Distance(t2.position, myTransform.position));
        });
    }

    // Runs the distance system, finds the closest player
    private void TargetEnemy()
    {
        if (!isServer)
        {
            return;
        }

        if (selectedTarget == null)
        {
            SortTargetByDistance();
            selectedTarget = targets[0];
        }

        else
        {
            int index = targets.IndexOf(selectedTarget);
        }
    }

    public void Update()
    {
        SortTargetByDistance();
        TargetEnemy();
    }

}