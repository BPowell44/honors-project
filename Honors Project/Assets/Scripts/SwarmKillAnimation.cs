﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SwarmKillAnimation : NetworkBehaviour 
{
    private MeleeEnemyHealth meleeEnemyHealth;
    private bool knockbackHappened = false;
    public float thrust = -15.0f;
    public Rigidbody rb;

	// Use this for initialization
	void Start () 
    {
        meleeEnemyHealth = GetComponent<MeleeEnemyHealth>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        DeathCheck();
	}

    public void DeathCheck()
    {
        if (meleeEnemyHealth.isDead)
        {
            if (!knockbackHappened)
            {
                knockbackHappened = true;
                rb.AddForce(transform.up * thrust);
            }
        }
    }
}