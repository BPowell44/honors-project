﻿using UnityEngine;
using System.Collections;

public class GrenadeThrow : MonoBehaviour 
{
    private Transform grenadeTransform;
    private Rigidbody grenadeRigidbody;
    private Vector3 shootDirection;

    public GameObject grenadePrefab;
    public Transform grenadeSpawn;

    public bool canBeShot;
    public float shotForce;

	// Use this for initialization
	void Start () 
    {
        grenadeTransform = transform;
        grenadeRigidbody = GetComponent<Rigidbody>();
        canBeShot = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
        CheckForInput();
	}

    void CheckForInput()
    {
        if (Input.GetKeyDown(KeyCode.G) && canBeShot)
        {
            ShootGrenade();
        }
    }

    void ShootGrenade()
    {
        var grenade = (GameObject)Instantiate(grenadePrefab, grenadeSpawn.position, grenadeSpawn.rotation);
        grenade.GetComponent<Rigidbody>().velocity = grenade.transform.forward * 50;
        canBeShot = false;  
    }


}
